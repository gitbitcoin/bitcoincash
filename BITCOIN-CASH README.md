    As with many development projects, this repository on Gitlab is considered to be the bitcoin-cash "staging" area for the latest changes. 
Anyone is welcome to contribute to b-cash codebase!
b-cash is a 100% open source endeavor. 
    To help faciliate broad community cooperation, a number of trusted Bitcoin-Cash community leaders have write permissions to the project's codebase,
allowing for decentralization and continuity. Community members, old and new, are encouraged to find ways to contribute to the success of the project. 
If you have experience with programming, product design, QA engineering, translation, or have a different set of skills that you want to bring to the project,
your involvement is appreciated!
===> join us!